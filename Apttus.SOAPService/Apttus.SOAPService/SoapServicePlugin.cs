﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Apttus.PluginBase;
using Apttus.SOAPService.Calculator;
using Microsoft.Xrm.Sdk;


namespace Apttus.SOAPService
{
    public class SoapServicePlugIn : PluginBase.PluginBase
    {

        public SoapServicePlugIn(string unsecureConfig, string secureConfig) : base(unsecureConfig, secureConfig)
        {
        }

        protected override void ExecuteCrmPlugin(LocalPluginContext localcontext)
        {
            //Extracting Service Details
            XmlDocument configDoc = new XmlDocument();
            configDoc.LoadXml(SecureConfig);
            localcontext.Trace("Strated Executing the Plugin");
            var serviceUrl = PluginConfiguration.GetConfigDataString(configDoc, "ServiceUrl");
            if (string.IsNullOrWhiteSpace(serviceUrl))
                throw new InvalidPluginExecutionException(String.Format(CultureInfo.InvariantCulture,
                      "Service Url is required!"));

            var serviceOperation = PluginConfiguration.GetConfigDataString(configDoc, "ServiceOperation");
            if (string.IsNullOrWhiteSpace(serviceOperation))
                throw new InvalidPluginExecutionException(String.Format(CultureInfo.InvariantCulture,
                      "Service operation is required!"));

            var serviceInputs = PluginConfiguration.GetConfigDataCollection(configDoc, "ServiceInputs");
            if (serviceInputs == null || serviceInputs.Count < 2)
                throw new InvalidPluginExecutionException(String.Format(CultureInfo.InvariantCulture,
                      "Two inputs are required!"));

            //Calling the service
            localcontext.Trace("Calling the Service");
            var binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            Uri uri = new Uri(serviceUrl);
            var endpoint = new EndpointAddress(uri);

            float result = callCalculatorService(binding, endpoint, serviceInputs, serviceOperation, localcontext);

            // For demonstration purposes, throw an exception so that the response
            // is shown in the trace dialog of the Microsoft Dynamics CRM user interface.
            throw new InvalidPluginExecutionException("SoapServicePlugin completed successfully with the result: " + result);
        }

        private float callCalculatorService(BasicHttpBinding binding, EndpointAddress endpoint, List<string> serviceInputs, string serviceOperation, PluginBase.PluginBase.LocalPluginContext localcontext)
        {
            float result = 0;
            localcontext.Trace("In the Service" + result);

            using (CalculatorClient client = new CalculatorClient(binding, endpoint))
            {
                switch (serviceOperation)
                {
                    case "add":
                        //result = await client.addAsync(float.Parse(serviceInputs[0]), float.Parse(serviceInputs[1]));
                        result = client.add(float.Parse(serviceInputs[0]), float.Parse(serviceInputs[1]));
                        localcontext.Trace("Add result" + result);
                        break;

                    case "divide":
                        //result = await client.divideAsync(float.Parse(serviceInputs[0]), float.Parse(serviceInputs[1]));
                        result = client.divide(float.Parse(serviceInputs[0]), float.Parse(serviceInputs[1]));
                        localcontext.Trace("divide result" + result);
                        break;

                    case "multiply":
                        //result = await client.multiplyAsync(float.Parse(serviceInputs[0]), float.Parse(serviceInputs[1]));
                        result = client.multiply(float.Parse(serviceInputs[0]), float.Parse(serviceInputs[1]));
                        localcontext.Trace("multiply result" + result);
                        break;

                    case "subtract":
                        //result = await client.subtractAsync(float.Parse(serviceInputs[0]), float.Parse(serviceInputs[1]));
                        result = client.subtract(float.Parse(serviceInputs[0]), float.Parse(serviceInputs[1]));
                        localcontext.Trace("Subtract result" + result);
                        break;
                }
            }
            return result;
        }
    }
}
