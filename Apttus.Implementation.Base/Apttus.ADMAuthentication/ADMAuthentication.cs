﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;

namespace Apttus.ADMAuthentication
{
    public class ADMAuthentication
    {
        public static string DatamarketAccessUri;
        private string _clientId;
        private string _clientSecret;
        private string _request;
        private ADMAccessToken _token;
        private Timer _accessTokenRenewer;

        //Access token expires every 10 minutes. Renew it every 9 minutes only.
        private const int RefreshTokenDuration = 9;
        public ADMAuthentication(string clientId, string clientSecret, string datamarketAccessUri, string resourceId)
        {
            _clientId = clientId;
            _clientSecret = clientSecret;
            
            //If clientid or client secret has special characters, encode before sending request
            _request = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&resource=" + resourceId, Uri.EscapeDataString(clientId), Uri.EscapeDataString(clientSecret));
            _token = HttpPost(datamarketAccessUri, _request);

            //renew the token every specfied minutes
            _accessTokenRenewer = new Timer(OnTokenExpiredCallback, this, TimeSpan.FromMinutes(RefreshTokenDuration), TimeSpan.FromMilliseconds(-1));
        }
        public ADMAccessToken GetAccessToken()
        {
            return _token;
        }
        private void RenewAccessToken()
        {
            ADMAccessToken newAccessToken = HttpPost(DatamarketAccessUri, _request);
            //swap the new token with old one
            //Note: the swap is thread unsafe
            _token = newAccessToken;
            Console.WriteLine("Renewed token for user: {0} is: {1}", _clientId, _token.AccessToken);
        }
        private void OnTokenExpiredCallback(object stateInfo)
        {
            try
            {
                RenewAccessToken();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                try
                {
                    _accessTokenRenewer.Change(TimeSpan.FromMinutes(RefreshTokenDuration), TimeSpan.FromMilliseconds(-1));
                }
                catch (Exception ex)
                {
                }
            }
        }
        private ADMAccessToken HttpPost(string datamarketAccessUri, string requestDetails)
        {
            //Prepare OAuth request 
            WebRequest webRequest = WebRequest.Create(datamarketAccessUri);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            byte[] bytes = Encoding.ASCII.GetBytes(requestDetails);
            webRequest.ContentLength = bytes.Length;
            using (Stream outputStream = webRequest.GetRequestStream())
            {
                outputStream.Write(bytes, 0, bytes.Length);
            }
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ADMAccessToken));
                //Get deserialized object from JSON stream
                ADMAccessToken token = (ADMAccessToken)serializer.ReadObject(webResponse.GetResponseStream());
                return token;

            }
        }
    }
}
