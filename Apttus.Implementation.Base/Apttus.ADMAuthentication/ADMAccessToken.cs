﻿using System.Runtime.Serialization;

namespace Apttus.ADMAuthentication
{
    [DataContract]
    public class ADMAccessToken
    {
        [DataMember]
        public string AccessToken { get; set; }
        [DataMember]
        public string TokenType { get; set; }
        [DataMember]
        public string ExpiresIn { get; set; }
        [DataMember]
        public string Scope { get; set; }
    }
}
