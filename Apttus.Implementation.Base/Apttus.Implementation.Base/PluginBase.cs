﻿using System;
using System.Globalization;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;

namespace Apttus.PluginBase
{
    /// <summary>
    /// Base Class for All Plugins
    /// </summary>
    public abstract class PluginBase : IPlugin
    {
        public class LocalPluginContext
        {
            public IServiceProvider ServiceProvider { get; private set; }

            /// <summary>
            /// The Microsoft Dynamics 365 organization service.
            /// </summary>

            public IOrganizationService OrganizationService { get; private set; }

            /// <summary>
            /// IPluginExecutionContext contains information that describes the run-time environment in which the plug-in executes, information related to the execution pipeline, and entity business information.
            /// </summary>
            public IPluginExecutionContext PluginExecutionContext { get; private set; }

            /// <summary>
            /// Synchronous registered plug-ins can post the execution context to the Microsoft Azure Service Bus. <br/> 
            /// It is through this notification service that synchronous plug-ins can send brokered messages to the Microsoft Azure Service Bus.
            /// </summary>
            public IServiceEndpointNotificationService NotificationService { get; private set; }

            /// <summary>
            /// Provides logging run-time trace information for plug-ins. 
            /// </summary>
            public ITracingService TracingService { get; private set; }

            private LocalPluginContext() { }

            /// <summary>
            /// Helper object that stores the services available in this plug-in.
            /// </summary>
            /// <param name="serviceProvider"></param>
            public LocalPluginContext(IServiceProvider serviceProvider)
            {
                if (serviceProvider == null)
                {
                    throw new InvalidPluginExecutionException("serviceProvider");
                }

                // Obtain the execution context service from the service provider.
                PluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                // Obtain the tracing service from the service provider.
                TracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

                // Get the notification service from the service provider.
                NotificationService = (IServiceEndpointNotificationService)serviceProvider.GetService(typeof(IServiceEndpointNotificationService));

                // Obtain the organization factory service from the service provider.
                IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

                // Use the factory to generate the organization service.
                OrganizationService = factory.CreateOrganizationService(PluginExecutionContext.UserId);
            }

            /// <summary>
            /// Writes a trace message to the CRM trace log.
            /// </summary>
            /// <param name="message">Message name to trace.</param>
            public void Trace(string message)
            {
                if (string.IsNullOrWhiteSpace(message) || TracingService == null)
                {
                    return;
                }

                if (this.PluginExecutionContext == null)
                {
                    this.TracingService.Trace(message);
                }
                else
                {
                    this.TracingService.Trace(
                        "{0}, Correlation Id: {1}, Initiating User: {2}",
                        message,
                        this.PluginExecutionContext.CorrelationId,
                        this.PluginExecutionContext.InitiatingUserId);
                }
            }
        }

        /// <summary>
        /// Secure Configuration passed from Plugin registration tool
        /// </summary>
        protected string SecureConfig { get; private set; }

        /// <summary>
        /// Unsecure Configuration passed from Plugin registration tool
        /// </summary>
        protected string UnsecureConfig { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginBase"/> class.
        /// </summary>
        /// <param name="unsecureConfig">Unsecure Config passed from Plugin Registration Tool</param>
        /// <param name="secureConfig">Secure Config passed from Plugin Registration Tool</param>
        protected PluginBase(string unsecureConfig, string secureConfig)
        {
            SecureConfig = secureConfig;
            UnsecureConfig = unsecureConfig;
        }

        /// <summary>
        /// Main entry point for he business logic that the plug-in is to execute.
        /// </summary>
        public void Execute(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
            {
                throw new InvalidPluginExecutionException("serviceProvider");
            }

            // Construct the local plug-in context.
            LocalPluginContext localcontext = new LocalPluginContext(serviceProvider);

            try
            {
                // Invoke the custom implementation 
                ExecuteCrmPlugin(localcontext);
                //return;
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                localcontext.Trace(string.Format(CultureInfo.InvariantCulture, "Exception: {0}", e.ToString()));

                // Handle the exception.
                HandleError(localcontext, e);
            }
            finally
            {
                localcontext.Trace("Exiting Execute()");
                Cleanup(localcontext);
            }
        }

        /// <summary>
        /// Placeholder to write cleanup code if required
        /// </summary>
        private void Cleanup(LocalPluginContext localcontext)
        {
            
        }

        /// <summary>
        /// PlaceHolder for Handling Exceptions
        /// </summary>
        /// <param name="localcontext"></param>
        /// <param name="e"></param>
        protected virtual void HandleError(LocalPluginContext localcontext, FaultException<OrganizationServiceFault> e)
        {
            throw e;
        }

        /// <summary>
        /// Placeholder for a custom plug-in implementation. 
        /// </summary>
        /// <param name="localcontext">Context for the current plug-in.</param>
        protected abstract void ExecuteCrmPlugin(LocalPluginContext localcontext);
    }
}
