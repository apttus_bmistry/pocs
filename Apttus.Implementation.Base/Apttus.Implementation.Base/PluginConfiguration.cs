﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Apttus.PluginBase
{
    public class PluginConfiguration
    {
        /// <summary>
        /// Get Value Node by passing a Config Doc and a key
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetValueNode(XmlDocument doc, string key)
        {
            XmlNode node = doc.SelectSingleNode(string.Format("Settings/setting[@name='{0}']", key));

            var selectSingleNode = node?.SelectSingleNode("value");
            if (selectSingleNode != null) return selectSingleNode.InnerText;
            return string.Empty;
        }

        /// <summary>
        /// Get Value Node collection by passing a Config Doc and a key
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static List<string> GetValueCollection(XmlDocument doc, string key)
        {
            XmlNode node = doc.SelectSingleNode(String.Format("Settings/setting[@name='{0}']", key));
            var result = new List<String>();
            var nodeCollection = node?.SelectNodes("value");
            if (nodeCollection != null && nodeCollection.Count > 0)
            {
                foreach (XmlNode valueNode in nodeCollection)
                {
                    result.Add(valueNode.InnerText);
                }
            }
            return result;
        }

        /// <summary>
        /// Get Config Data as GUID by passing a Config Doc and an entry label 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public static Guid GetConfigDataGuid(XmlDocument doc, string label)
        {
            string tempString = GetValueNode(doc, label);

            if (tempString != string.Empty)
            {
                return new Guid(tempString);
            }
            return Guid.Empty;
        }

        /// <summary>
        /// Get Config Data as Boolean by passing a Config Doc and an entry label
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public static bool GetConfigDataBool(XmlDocument doc, string label)
        {
            bool retVar;

            if (bool.TryParse(GetValueNode(doc, label), out retVar))
            {
                return retVar;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get Config Data as Integer by passing a COnfig Doc and an entry label
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public static int GetConfigDataInt(XmlDocument doc, string label)
        {
            int retVar;

            if (int.TryParse(GetValueNode(doc, label), out retVar))
            {
                return retVar;
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Get Config Data String by passing a config doc and a label
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public static string GetConfigDataString(XmlDocument doc, string label)
        {
            return GetValueNode(doc, label);
        }


        //Get Config Data Collection by passing a config doc and a label
        public static List<string> GetConfigDataCollection(XmlDocument doc, string label)
        {
            return GetValueCollection(doc, label);
        }
    }
}
